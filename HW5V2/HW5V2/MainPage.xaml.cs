﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json;
using HW5V2.Models;


namespace HW5V2
{
    public partial class MainPage : ContentPage
    {
        public List<ProductsInfo> productDatafromJson;
        public MainPage()
        {
            InitializeComponent();
            ReadInJsonFile();
        }

        private void ReadInJsonFile()
        {
            var fileName = "HW5.products.json";

            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(fileName);

            ProductsInfo data;
            using (var reader = new System.IO.StreamReader(stream))
            {
                var jsonAsString = reader.ReadToEnd();
                productDatafromJson = JsonConvert.DeserializeObject<List<ProductsInfo>>(jsonAsString);
            }
            ProductListView.ItemsSource = productDatafromJson;



        }

        async void MenuItem_Clicked(object sender, EventArgs e)
        {
            var item = (MenuItem)sender;
            var itemSelected = item.CommandParameter as ProductsInfo;
            await Navigation.PushAsync(new More(itemSelected));

        }
    }
}


