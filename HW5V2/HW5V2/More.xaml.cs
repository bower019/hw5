﻿using HW5V2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW5V2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class More : ContentPage
	{
		public More ()
		{
			InitializeComponent ();
		}

        public More(ProductsInfo Info)
        {
            InitializeComponent();
            BindingContext = Info;
        }
    }
}