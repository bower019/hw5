﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Xamarin.Forms;
using HW5V2;

namespace HW5V2.Models
{
	public class ProductsInfo 
	{
        public partial class ProductsData
        {
            
            public double productName { get; set; }

            
            public double price { get; set; }

            
            public double starRating { get; set; }
       
       
            public double productCode { get; set; }

           
            public double releaseDate { get; set; }

          
            public double description { get; set; }

           
            public double imageUrl { get; set; }
        }

    }
}